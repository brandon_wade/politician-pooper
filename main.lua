require "menu"
require "game"

function love.load()  
  --shortcuts
  gr = love.graphics
  au = love.audio
  kb = love.keyboard
  font = love.graphics.newFont("/font/kenvector_future.ttf", 16)
  
  -- audio
  mbgm = au.newSource("/audio/menu.mp3")
  mbgm:setLooping (true)
  gbgm = au.newSource("/audio/zone.mp3")
  gbgm:setLooping (true)
  obgm = au.newSource("/audio/over.mp3")
  obgm:setLooping (false)
  bsfx = au.newSource("/audio/splatt.mp3")
  bsfx:setLooping (false)
  
  gamestate = "menu"
  
  button_spawn(230,300,"Start","start")
  button_spawn(500,300,"Quit","quit")

  color = 40
  
  LoadGame()
  menu_load()
  mouseStartY = 0
  mouseEndY = 0
  mouseDown = false
  gesture = true
end

function love.update(dt)
  if gamestate == "menu" then
    menu_loop()
  end
  if gamestate == "tutorial" then
    --menu_loop()
  end
  if gamestate == "play" then
    gameLoop()
  end
  
end

function love.draw()
  
  if gamestate == "menu" then
    gr.setBackgroundColor(20,20,100)
    menu_draw()
    gr.print("POLITICAN POOPER",220,100,0,2)
    button_draw()
    au.stop (gbgm)
    au.stop (obgm)    
    au.play (mbgm)
       
  end
  if gamestate == "tutorial" then
    gr.setBackgroundColor(0,0,0,50)
    menu_draw()
    gr.print("h - For help ingame",10,10)
    gr.print("lmb - poop on politicians",10,30)
    gr.print("drag lmb up and down to switch lanes",10,50)
    gr.print("Hit Enter to start the game!",250,120)
    if kb.isDown("return") then
      gamestate = "play"
    end
  end
  if gamestate == "help" then
    menu_draw()
    gr.print("h - For help ingame",10,10)
    gr.print("lmb - poop on politicians",10,30)
    gr.print("drag lmb up and down to switch lanes",10,50)
    gr.print("Hit Enter to return to the game!",250,120)
    if kb.isDown("return") then
      gamestate = "play"
    end
  end
  if gamestate == "play" then
    gameDraw()
    au.stop (mbgm)
    au.play (gbgm)
    if kb.isDown("d") then
      gamestate = "over"
    elseif kb.isDown("h") then
      gamestate = "help"
    end
  end
  if gamestate == "over" then
    gr.setBackgroundColor(0,0,0)
    gr.print("Game Over!",350,200)
    gr.print("Press Return for Menu",290,300)
    gr.print("Score: "..score, 370, 250)
    if love.keyboard.isDown("return") then
      gamestate = "menu"
      au.stop (obgm)
    end
    au.stop (gbgm)
    au.play (obgm)
    obgm:setLooping (false)
  end
end

function love.mousepressed(x,y)
  if gamestate == "menu" then
    button_click(x,y)
  end
   if gamestate == "play" then
    mouseStartY = y
  end
end

function love.mousereleased(x, y, button, istouch)
  
  mouseEndY = mouseStartY - y
    
  if mouseEndY < -20 then
    if birdY < 76 then
      textString = "down" 
      birdY = birdY + 25
    end
  
  elseif mouseEndY > 20 then
    if birdY > 1 then
      textString = "up" 
      birdY = birdY - 25
    end
  
else
  if poopActive == false then
    textString = "touch"
    poopActive = true
    poopX = 70
    poopY = birdY + 15
  end
  
end

end