
function LoadGame()
  math.randomseed(os.time())
  
  bird = love.graphics.newImage("images/gull2.png")
  bg = love.graphics.newImage("images/bg2.png")
  ground1 = love.graphics.newImage("images/ground.png")
  ground2 = love.graphics.newImage("images/ground.png")
  
  redBar = love.graphics.newImage("images/redbar.png")
  greyBar = love.graphics.newImage("images/greybar.png")
  poop = love.graphics.newImage("images/poop.png")
  
  person1 = love.graphics.newImage("images/theresa.png")
  person2 = love.graphics.newImage("images/boris.png")
  person3 = love.graphics.newImage("images/camreon.png")
  person4 = love.graphics.newImage("images/clegg.png")
  person5 = love.graphics.newImage("images/farage.png")
  
  person1Poop = love.graphics.newImage("images/theresaPoop.png")
  person2Poop = love.graphics.newImage("images/borisPoop.png")
  person3Poop = love.graphics.newImage("images/camreonPoop.png")
  person4Poop = love.graphics.newImage("images/cleggPoop.png")
  person5Poop = love.graphics.newImage("images/faragePoop.png")
  
  person1X = 800
  person2X = 1200
  person3X = 1600
  person4X = 2000
  person5X = 2400
  
  person1Draw = true;
  person2Draw = true;
  person3Draw = true;
  person4Draw = true;
  person5Draw = true;
  
  obsticle1 = love.graphics.newImage("images/enemy.png")
  obsticle2 = love.graphics.newImage("images/enemy.png")
  obsticle3 = love.graphics.newImage("images/enemy.png")
  obsticle4 = love.graphics.newImage("images/enemy.png")
  obsticle5 = love.graphics.newImage("images/enemy.png")
  
  obsticle1X = math.random(800, 2400)
  obsticle1Y = ((math.random(0, 3) * 25) + 2)
  obsticle2X = math.random(800, 2400)
  obsticle2Y = ((math.random(0, 3) * 25) + 2)
  obsticle3X = math.random(800, 2400)
  obsticle3Y = ((math.random(0, 3) * 25) + 2)
  obsticle4X = math.random(800, 2400)
  obsticle4Y = ((math.random(0, 3) * 25) + 2)
  obsticle5X = math.random(800, 2400)
  obsticle5Y = ((math.random(0, 3) * 25) + 2)
  
  birdY = 1
  ground1Pos = 0
  ground2Pos = 800
  
  poopX = 0
  poopY = 0
  poopActive = false
  
  textString = "some text"
  score = 0

end


function gameLoop()
  if poopActive == true then
    collisionDetection()
  end
  
  birdCollsion()
  
  ground1Pos = ground1Pos - 1
  ground2Pos = ground2Pos - 1
  
  if ground1Pos < -800 then
    ground1Pos = 800
  end
  if ground2Pos < -800 then
    ground2Pos = 800
  end
  
  if poopActive == true then   
    poopX = poopX + 1
    poopY = poopY + 2   
    if poopY > 400 then
      poopActive = false
    end
    
  end
  
  person1X = person1X -1
  person2X = person2X -1
  person3X = person3X -1
  person4X = person4X -1
  person5X = person5X -1
  
  if person1X < -100 then
    person1Draw = true;
    person1X = 1900
  end
  if person2X < -100 then
    person2Draw = true;
    person2X = 1900
  end
  if person3X < -100 then
    person3Draw = true;
    person3X = 1900
  end
  if person4X < -100 then
    person4Draw = true;
    person4X = 1900
  end
  if person5X < -100 then
    person5Draw = true;
    person5X = 1900
  end
  
  obsticle1X = obsticle1X - 2
  obsticle2X = obsticle2X - 2
  obsticle3X = obsticle3X - 2
  obsticle4X = obsticle4X - 2
  obsticle5X = obsticle5X - 2
  
  if obsticle1X < -30 then
    obsticle1X = math.random(800, 2400)
    obsticle1Y = ((math.random(0, 3) * 25) + 2)
  end
  if obsticle2X < -30 then
    obsticle2X = math.random(800, 2400)
    obsticle2Y = ((math.random(0, 3) * 25) + 2)
  end
  if obsticle3X < -30 then
    obsticle3X = math.random(800, 2400)
    obsticle3Y = ((math.random(0, 3) * 25) + 2)
  end
  if obsticle4X < -30 then
    obsticle4X = math.random(800, 2400)
    obsticle4Y = ((math.random(0, 3) * 25) + 2)
  end
  if obsticle5X < -30 then
    obsticle5X = math.random(800, 2400)
    obsticle5Y = ((math.random(0, 3) * 25) + 2)
  end
  
  
end


function gameDraw()
  love.graphics.draw(bg, 0, 0)
  love.graphics.draw(ground1, ground1Pos, 350)
  love.graphics.draw(ground2, ground2Pos, 350)
  
  love.graphics.print("Hits: " .. score,10,love.graphics.getHeight()-20)
  
  love.graphics.draw(redBar, 0, 0)
  love.graphics.draw(greyBar, 0, 25)
  love.graphics.draw(redBar, 0, 50)
  love.graphics.draw(greyBar, 0, 75)
  
  if person1Draw == true then
    love.graphics.draw(person1, person1X, 295)
  else
    love.graphics.draw(person1Poop, person1X, 295)
  end
  if person2Draw == true then
    love.graphics.draw(person2, person2X, 295)
    else
    love.graphics.draw(person2Poop, person2X, 295)
  end
  if person3Draw == true then
    love.graphics.draw(person3, person3X, 295)
    else
    love.graphics.draw(person3Poop, person3X, 295)
  end
  if person4Draw == true then
    love.graphics.draw(person4, person4X, 295)
    else
    love.graphics.draw(person4Poop, person4X, 295)
  end
  if person5Draw == true then
    love.graphics.draw(person5, person5X, 295)
    else
    love.graphics.draw(person5Poop, person5X, 295)
  end
  
  love.graphics.draw(obsticle1, obsticle1X, obsticle1Y)
  love.graphics.draw(obsticle2, obsticle2X, obsticle2Y)
  love.graphics.draw(obsticle3, obsticle3X, obsticle3Y)
  love.graphics.draw(obsticle4, obsticle4X, obsticle4Y)
  love.graphics.draw(obsticle5, obsticle5X, obsticle5Y)
  
  if poopActive == true then
    love.graphics.draw(poop, poopX, poopY)    
  end
  
  love.graphics.draw(bird, 50, birdY)

end

function collisionDetection()
  if poopX > person1X and poopX < (person1X + 74) and poopY > 295 and poopY < 480 then
    score = score + 1
    poopActive = false
    person1Draw = false
  end
  if poopX > person2X and poopX < (person2X + 100) and poopY > 295 and poopY < 480 then
    score = score + 1
    poopActive = false
    person2Draw = false
  end
  if poopX > person3X and poopX < (person3X + 31) and poopY > 295 and poopY < 480 then
    score = score + 1
    poopActive = false
    person3Draw = false
  end
  if poopX > person4X and poopX < (person4X + 35) and poopY > 295 and poopY < 480 then
    score = score + 1
    poopActive = false
    person4Draw = false
  end
  if poopX > person5X and poopX < (person5X + 44) and poopY > 295 and poopY < 480 then
    score = score + 1
    poopActive = false
    person5Draw = false
  end
end

function birdCollsion()
  if obsticle1X > 50 and obsticle1X < 113 and obsticle1Y > birdY and obsticle1Y < (birdY + 23) then
    gamestate = "over"
  end
  if obsticle2X > 50 and obsticle2X < 113 and obsticle2Y > birdY and obsticle2Y < (birdY + 23) then
    gamestate = "over"
  end
  if obsticle3X > 50 and obsticle3X < 113 and obsticle3Y > birdY and obsticle3Y < (birdY + 23) then
    gamestate = "over"
  end
  if obsticle4X > 50 and obsticle4X < 113 and obsticle4Y > birdY and obsticle4Y < (birdY + 23) then
    gamestate = "over"
  end
  if obsticle5X > 50 and obsticle5X < 113 and obsticle5Y > birdY and obsticle5Y < (birdY + 23) then
    gamestate = "over"
  end
  
end



