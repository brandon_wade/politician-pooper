
button = {}
gr = love.graphics

function button_spawn(x,y,text,id)
  table.insert(button, {x=x,y=y,text=text,id=id})
end

function button_draw()
  -- Sets the colour and font for every button and prints it
  for i,v in ipairs(button) do
    gr.setColor(255,255,255)
    gr.setFont(font)
    gr.print(v.text,v.x,v.y)
  end
end

function button_click(x,y)
  --Checks to see if the x,y of the mouse is within bounds of the x,y of the button
  for i,v in ipairs(button) do
    if x > v.x and
    x < v.x + font:getWidth(v.text) and
    y > v.y and
    y < v.y + font:getHeight(v.text) then
      if v.id == "quit" then
        love.event.push("quit")
      end
      if v.id == "start" then
        gamestate = "tutorial"
      end
    end
  end
end

function menu_load()
  bg = gr.newImage("images/bg2.png")
  ground1 = gr.newImage("images/ground.png")
  ground2 = gr.newImage("images/ground.png")
end

function menu_loop()
  ground1Pos = ground1Pos - 1
  ground2Pos = ground2Pos - 1
  
  if ground1Pos < -800 then
    ground1Pos = 800
  end
  if ground2Pos < -800 then
    ground2Pos = 800
  end
end

function menu_draw()
  gr.draw(bg, 0, 0)
  gr.draw(ground1, ground1Pos, 350)
  gr.draw(ground2, ground2Pos, 350)
end